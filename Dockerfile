FROM newtmitch/sonar-scanner:alpine

ARG ARTIFACTORY_API_KEY_BITBUCKET_USER

RUN curl -H "X-JFrog-Art-Api: ${ARTIFACTORY_API_KEY_BITBUCKET_USER}" -O "https://artifactory.indigint.net/artifactory/certificates/sonarcert.cer" &&\
  keytool -import -noprompt -trustcacerts -alias indigosonar -file sonarcert.cer -keystore /usr/lib/jvm/java-1.8-openjdk/jre/lib/security/cacerts -storepass changeit

COPY pipe.sh /

ENTRYPOINT ["/pipe.sh"]
