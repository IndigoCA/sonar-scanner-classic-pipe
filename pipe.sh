#!/bin/bash

set -e

parse_environment_variables() {
  EXTRA_ARGS=${EXTRA_ARGS:=""}
  SERVER=${SERVER:="https://sonarqube.qa.cloudapps.indigint.net/"}
  SOURCE_DIRECTORY=${SOURCE_DIRECTORY:=""}
  BRANCH=${BRANCH:?'BRANCH variable missing.'}
  VERSION=${VERSION:?'VERSION variable missing.'}
  LOGIN=${LOGIN:?'LOGIN variable missing.'}
  PASSWORD=${PASSWORD:?'PASSWORD variable missing.'}
}

parse_environment_variables

echo sonar-scanner -Dsonar.branch=${BRANCH} -Dsonar.projectVersion=${VERSION} -Dsonar.host.url=${SERVER} -Dsonar.login=***** -Dsonar.password=***** -Dsonar.projectBaseDir=./src/${SOURCE_DIRECTORY} ${EXTRA_ARGS}

sonar-scanner -Dsonar.branch=${BRANCH} -Dsonar.projectVersion=${VERSION} -Dsonar.host.url=${SERVER} -Dsonar.login=${LOGIN} -Dsonar.password=${PASSWORD} -Dsonar.projectBaseDir=./src/${SOURCE_DIRECTORY} ${EXTRA_ARGS}
